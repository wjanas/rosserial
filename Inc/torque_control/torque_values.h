#ifndef _ROS_torque_control_torque_values_h
#define _ROS_torque_control_torque_values_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"
#include "std_msgs/Header.h"

namespace torque_control
{

  class torque_values : public ros::Msg
  {
    public:
      typedef std_msgs::Header _header_type;
      _header_type header;
      typedef float _motor_1_1_type;
      _motor_1_1_type motor_1_1;
      typedef float _motor_1_2_type;
      _motor_1_2_type motor_1_2;
      typedef float _motor_2_1_type;
      _motor_2_1_type motor_2_1;
      typedef float _motor_2_2_type;
      _motor_2_2_type motor_2_2;
      typedef float _motor_3_1_type;
      _motor_3_1_type motor_3_1;
      typedef float _motor_3_2_type;
      _motor_3_2_type motor_3_2;
      typedef float _motor_4_1_type;
      _motor_4_1_type motor_4_1;
      typedef float _motor_4_2_type;
      _motor_4_2_type motor_4_2;

    torque_values():
      header(),
      motor_1_1(0),
      motor_1_2(0),
      motor_2_1(0),
      motor_2_2(0),
      motor_3_1(0),
      motor_3_2(0),
      motor_4_1(0),
      motor_4_2(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      offset += this->header.serialize(outbuffer + offset);
      union {
        float real;
        uint32_t base;
      } u_motor_1_1;
      u_motor_1_1.real = this->motor_1_1;
      *(outbuffer + offset + 0) = (u_motor_1_1.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_motor_1_1.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_motor_1_1.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_motor_1_1.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->motor_1_1);
      union {
        float real;
        uint32_t base;
      } u_motor_1_2;
      u_motor_1_2.real = this->motor_1_2;
      *(outbuffer + offset + 0) = (u_motor_1_2.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_motor_1_2.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_motor_1_2.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_motor_1_2.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->motor_1_2);
      union {
        float real;
        uint32_t base;
      } u_motor_2_1;
      u_motor_2_1.real = this->motor_2_1;
      *(outbuffer + offset + 0) = (u_motor_2_1.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_motor_2_1.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_motor_2_1.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_motor_2_1.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->motor_2_1);
      union {
        float real;
        uint32_t base;
      } u_motor_2_2;
      u_motor_2_2.real = this->motor_2_2;
      *(outbuffer + offset + 0) = (u_motor_2_2.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_motor_2_2.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_motor_2_2.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_motor_2_2.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->motor_2_2);
      union {
        float real;
        uint32_t base;
      } u_motor_3_1;
      u_motor_3_1.real = this->motor_3_1;
      *(outbuffer + offset + 0) = (u_motor_3_1.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_motor_3_1.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_motor_3_1.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_motor_3_1.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->motor_3_1);
      union {
        float real;
        uint32_t base;
      } u_motor_3_2;
      u_motor_3_2.real = this->motor_3_2;
      *(outbuffer + offset + 0) = (u_motor_3_2.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_motor_3_2.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_motor_3_2.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_motor_3_2.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->motor_3_2);
      union {
        float real;
        uint32_t base;
      } u_motor_4_1;
      u_motor_4_1.real = this->motor_4_1;
      *(outbuffer + offset + 0) = (u_motor_4_1.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_motor_4_1.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_motor_4_1.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_motor_4_1.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->motor_4_1);
      union {
        float real;
        uint32_t base;
      } u_motor_4_2;
      u_motor_4_2.real = this->motor_4_2;
      *(outbuffer + offset + 0) = (u_motor_4_2.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_motor_4_2.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_motor_4_2.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_motor_4_2.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->motor_4_2);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      offset += this->header.deserialize(inbuffer + offset);
      union {
        float real;
        uint32_t base;
      } u_motor_1_1;
      u_motor_1_1.base = 0;
      u_motor_1_1.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_motor_1_1.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_motor_1_1.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_motor_1_1.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->motor_1_1 = u_motor_1_1.real;
      offset += sizeof(this->motor_1_1);
      union {
        float real;
        uint32_t base;
      } u_motor_1_2;
      u_motor_1_2.base = 0;
      u_motor_1_2.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_motor_1_2.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_motor_1_2.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_motor_1_2.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->motor_1_2 = u_motor_1_2.real;
      offset += sizeof(this->motor_1_2);
      union {
        float real;
        uint32_t base;
      } u_motor_2_1;
      u_motor_2_1.base = 0;
      u_motor_2_1.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_motor_2_1.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_motor_2_1.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_motor_2_1.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->motor_2_1 = u_motor_2_1.real;
      offset += sizeof(this->motor_2_1);
      union {
        float real;
        uint32_t base;
      } u_motor_2_2;
      u_motor_2_2.base = 0;
      u_motor_2_2.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_motor_2_2.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_motor_2_2.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_motor_2_2.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->motor_2_2 = u_motor_2_2.real;
      offset += sizeof(this->motor_2_2);
      union {
        float real;
        uint32_t base;
      } u_motor_3_1;
      u_motor_3_1.base = 0;
      u_motor_3_1.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_motor_3_1.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_motor_3_1.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_motor_3_1.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->motor_3_1 = u_motor_3_1.real;
      offset += sizeof(this->motor_3_1);
      union {
        float real;
        uint32_t base;
      } u_motor_3_2;
      u_motor_3_2.base = 0;
      u_motor_3_2.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_motor_3_2.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_motor_3_2.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_motor_3_2.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->motor_3_2 = u_motor_3_2.real;
      offset += sizeof(this->motor_3_2);
      union {
        float real;
        uint32_t base;
      } u_motor_4_1;
      u_motor_4_1.base = 0;
      u_motor_4_1.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_motor_4_1.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_motor_4_1.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_motor_4_1.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->motor_4_1 = u_motor_4_1.real;
      offset += sizeof(this->motor_4_1);
      union {
        float real;
        uint32_t base;
      } u_motor_4_2;
      u_motor_4_2.base = 0;
      u_motor_4_2.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_motor_4_2.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_motor_4_2.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_motor_4_2.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->motor_4_2 = u_motor_4_2.real;
      offset += sizeof(this->motor_4_2);
     return offset;
    }

    const char * getType(){ return "torque_control/torque_values"; };
    const char * getMD5(){ return "a28448c4ea03bdff24b8fd873461f7f7"; };

  };

}
#endif
